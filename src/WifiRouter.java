/**
 * Class that controls the WifiRouter.
 * Sends requests to display weather information.
 * @author Ashlyn Weeks
 *
 */
public class WifiRouter {
	
	/** Status of push request. */
	private boolean myPush;
	
	public void WifiRouter() {
		
	}
	/**
	 * Checks if the push request was sent or not.
	 * @return boolean that displays push status.
	 */
	public boolean pushisConfirmed() {
		if(myPush) {
		
			return true;
			
		} else {
		
			return false;
			
		}
	}
	/**
	 * Is supposed to send a signal to 
	 * the wifi router. However we have no router to 
	 * send a request to here. 
	 */
	public void sendPushRequest() {
			
	}
	
	/**
	 * If connection to the router was unsuccessful 
	 * retry sending push request. 
	 */
	public void retryPushRequest() {
	
		
	}
		
}
