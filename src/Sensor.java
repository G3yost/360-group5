import java.time.Instant;

/**
 * Sensor represents an instance of sensory data collected from the physical sensors
 * of the weather station. A single instance is a given location and date. The types
 * of data includes: location, data, the moon phase, rainfall rate, temperature,
 * pressure, wind speed, and humidity.
 * 
 * @author Sam Wainright
 * @version 1.05, 12/11/2019
 */
public class Sensor {
	
	private String temperatureUnit = "celcius";
	private String rainRateUnit = "millimetres per hour";
	private String pressureUnit = "pascals";
	private String windSpeedUnit = "meters per second";
	private String humidityUnit = "grams";
	
	private Instant date;
	private String location;
	private String moonPhase;
	private double rainRate;
	private double temperature;
	private double pressure;
	private double windSpeed;
	private double humidity;
	
	public Sensor () {
		
		this.date = Instant.now();
		this.location = "DEFAULT";
		this.moonPhase = "CONSTRUCTOR";
		this.rainRate = 0;
		this.temperature = 0;
		this.pressure = 0;
		this.windSpeed = 0;
		this.humidity = 0;
	}
	public Sensor(Instant newDate,
			String newLocation,
			String newMoonPhase,
			double newRainRate,
			double newTemp,
			double newPressure,
			double newWindSpeed,
			double newHumidity) {
		
		this.date = newDate;
		this.location = newLocation;
		this.moonPhase = newMoonPhase;
		this.rainRate = newRainRate;
		this.temperature = newTemp;
		this.pressure = newPressure;
		this.windSpeed = newWindSpeed;
		this.humidity = newHumidity;
		
	}
	
	public Instant getDate() {
		return this.date;
	}
	
	public String getLocation() {
		return this.location;
	}
	
	public String moonPhase() {
		return this.moonPhase;
	}
	
	public double getRainRate() {
		return this.rainRate;
	}
	
	public double getTemperature() {
		return this.temperature;
	}
	
	public double getPressure() {
		return this.pressure;
	}
	
	public double getWindSpeed() {
		return this.windSpeed;
	}
	
	public double getHumidity( ) {
		return this.humidity;
	}
	
	public String getMoonPhase() {
		return this.moonPhase;
	}
	
	public void setMoonPhase(String newMoonPhase) {
		this.moonPhase = newMoonPhase;
	}
	
	/**
	 * Converts the temperature to an SI unit of measurement if
	 * currently imperial, and vice-versa. Conversion of Celcius to 
	 * Fahrenheit is (Celcius * (9/5)) + 32. Conversion of Fahrenheit to 
	 * Celcius is (Fahrenheit - 32) * (5/9).
	 * 
	 * @param newUnit The new measurement unit in Fahrenheit or Celcius.
	 */
	public void convertTemperatureUnit(String newUnit) {
		switch(newUnit) {
			case "fahrenheit":
				if (this.temperatureUnit.equals("celcius")) {
					this.temperatureUnit = newUnit;
					this.temperature = (this.temperature * (9/5)) + 32;
				} else if (this.temperatureUnit.equals("fahrenheit")) {
					this.temperatureUnit = newUnit;
				}
				break;
			case "celcius":
				if (this.temperatureUnit.equals("fahrenheit")) {
					this.temperatureUnit = newUnit;
					this.temperature = (this.temperature - 32) * (5/9);
				} else if (this.temperatureUnit.equals("celcius")) {
					this.temperatureUnit = newUnit;
				}
				break;
			default:
				System.out.println("Invalid temperature measurement unit."); // Throw error on display
				break;
		}
	}
	
	/**
	 * Converts the current rainfall measurement unit to a new one.
	 * If rainfall is measured in Metric units, converts to Imperial, 
	 * and vice-versa. The conversion rate is 1 millimetres per hour =
	 * 0.0393701 inches per hour.
	 * 
	 * @param newUnit The new measurement unit for rainfall in millimetres per hour or inches per hour.
	 */
	public void convertRainRateUnit(String newUnit) {
		switch(newUnit) {
		case "inches per hour":
			if(this.rainRateUnit.equals("millimetres per hour")) {
				this.rainRateUnit = newUnit;
				this.rainRate = this.rainRate / 25.4;
			} else if(this.rainRateUnit.equals("inches per hour")) {
				this.rainRateUnit = newUnit;
			}
			break;
		case "millimetres per hour":
			if(this.rainRateUnit.equals("inches per hour")) {
				this.rainRateUnit = newUnit;
				this.rainRate = this.rainRate * 25.4;
			} else if(this.rainRateUnit.equals("millimetres per hour")) {
				this.rainRateUnit = newUnit;
			}
			break;
			
		default:
			System.out.println("Invalid rainfall measurement unit."); // Throw error on display
			break;
		}
	}
	
	/**
	 * Converts the current pressure measurement unit to a new one.
	 * If pressure is measured in SI units, converts to Imperial, 
	 * and vice-versa. The conversion rate is 1 pascal =
	 * 0.00014503773 pounds per square inch.
	 * 
	 * @param newUnit The new measurement unit for pressure in pascals or pounds per square inch.
	 */
	public void convertPressureUnit(String newUnit) {
		switch(newUnit) {
			case "pounds per square inch":
				if(this.pressureUnit.equals("pascals")) {
					this.pressureUnit = newUnit;
					this.pressure = this.pressure * 0.00014503773;
				} else if(this.pressureUnit.equals("pounds per square inch")) {
					this.pressureUnit = newUnit;
				}
				break;
			case "pascals":
				if(this.pressureUnit.equals("pounds per square inch")) {
					this.pressureUnit = newUnit;
					this.pressure = this.pressure / 0.00014503773;
				} else if(this.pressureUnit.equals("pascals")) {
					this.pressureUnit = newUnit;
				}
				break;
				
			default:
				System.out.println("Invalid pressure measurement unit."); // Throw error on display
				break;
		}
	}
	
	/**
	 * Converts the current wind speed measurement unit to a new one.
	 * If the wind speed is measured in Metric units, converts to Imperial, 
	 * and vice-versa. The conversion rate is 1 knot =
	 * 0.51444 meters per second.
	 * 
	 * @param newUnit The new measurement unit for pressure in knots or meters per second.
	 */
	public void convertWindSpeedUnit(String newUnit) {
		switch(newUnit) {
			case "knots":
				if (this.windSpeedUnit.equals("meters per second")) {
					this.windSpeedUnit = newUnit;
					this.windSpeed = this.windSpeed / 0.51444;
				} else if (this.windSpeedUnit.equals("knots")) {
					this.windSpeedUnit = newUnit;
				}
				break;
			case "meters per second":
				if (this.windSpeedUnit.equals("knots")) {
					this.windSpeedUnit = newUnit;
					this.windSpeed = this.windSpeed * 0.51444;
				} else if (this.windSpeedUnit.equals("meters per second")) {
					this.windSpeedUnit = newUnit;
				}
				break;
			default:
				System.out.println("Invalid wind speed measurement unit."); // Throw error on display
				break;
		}
	}
	public int compareTo(Sensor sensorFinal) {
		// TODO Auto-generated method stub
		
		if (this.rainRate > sensorFinal.rainRate)
			return 1;
		else if (this.rainRate < sensorFinal.rainRate)
			return -1;
		else return 0;
	}
}
