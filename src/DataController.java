/*
 * @author Samuel Schuler
 * TCSS360 Project 4
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.temporal.TemporalField;

/**
 * Data Controller specifies an array of sensors and checks them for 
 * specific data that should be analyzed by other programs.
 */
public class DataController {
	
	private Sensor[] mySensorArray;
	private final static int SIZE = 10;
	
	/**
	 * constructor for data controller
	 */
	public DataController() { 
		mySensorArray = new Sensor[SIZE]; 
	}
	
	/**
	 * finds weather at a certain date.
	 * @param theDate date that weather should be found at.
	 * @return sensor at certain date.
	 */
	public Sensor getWeatherByDate(Instant theDate) {
		Sensor retDate = new Sensor();
		int n = mySensorArray.length;
		for(int i = 0; i < n - 1; i++) {
			if(mySensorArray[i].getDate().equals(theDate)) {
				retDate = mySensorArray[i]; 
			}
		}
		return retDate;
	}
	
	/**
	 * finds weather data points within the time range.
	 * @param theTime range of times to look at
	 * @return sensors within the specified time
	 */
	public Sensor getWeatherByRange(TemporalField theTime) {
		Sensor retDate = new Sensor();
		int n = mySensorArray.length;
		for(int i = 0; i < n - 1; i++) {
			if(mySensorArray[i].getDate().isSupported(theTime)) {
				retDate = mySensorArray[i]; 
			}
		}
		return retDate;
	}
	
	/**
	 * return index sensor data
	 * @param index sensor to look at
	 * @return sensor at index
	 */
	public Sensor getWeather(int index) {
		return mySensorArray[index];
	}
	
	/**
	 * returns the sensor data
	 * @return sensor data
	 */
	public Sensor[] getAllWeather() {
		return mySensorArray;
	}
	
	/**
	 * sortWeatherByDate uses a simple bubble sort to sort the weather by date.
	 */
	public void sortWeatherByDate() {
		int n = mySensorArray.length;
		for(int i = 0; i < n - 1; i ++) {
			for(int j = 0; j < n - i - 1; j++) {
				if(mySensorArray[j - 1].getDate().compareTo(mySensorArray[j].getDate()) > 0) {
					Sensor temp = new Sensor();
					temp = mySensorArray[j - 1];
					mySensorArray[j - 1] = mySensorArray[j];
					mySensorArray[j] = temp;
				}
			}
		}
		
	}
	
	/**
	 * finds the highest weather by some metric defined by the compare method
	 * @return the highest weather data point
	 */
	public Sensor findHigh() {
		int n = mySensorArray.length;
		Sensor sensorFinal = new Sensor();
		for(int i = 0; i < n - 1; i++) {
			if(mySensorArray[i].compareTo(sensorFinal) > 0) {
				sensorFinal = mySensorArray[i];
			}
		}
		return sensorFinal;
	}
	
	/**
	 * finds the lowest weather by some metric defined by the compare method
	 * @return the lowest weather data point
	 */
	public Sensor findLow() {
		int n = mySensorArray.length;
		Sensor sensorFinal = new Sensor();
		for(int i = 0; i < n - 1; i++) {
			if(mySensorArray[i].compareTo(sensorFinal) < 0) {
				sensorFinal = mySensorArray[i];
			}
		}
		return sensorFinal;
	}
	
	/**
	 * gets an instance of this object
	 * @return this object
	 */
	public DataController export() {
		return this;
	}
	
	/**
	 * returns an instance of this object specified by TemporalField
	 * @param theTime time that should be accepted
	 * @return this object
	 */
	public DataController exportByRange(TemporalField theTime) {
		Sensor retDate = new Sensor();
		int n = mySensorArray.length;
		for(int i = 0; i < n - 1; i++) {
			if(!mySensorArray[i].getDate().isSupported(theTime)) {
				mySensorArray[i] = null; 
			}
		}
		return this;
	}
	
	/**
	 * writes weather data to a file 
	 * @param theFile file to write to
	 */
	public void parseReport(File theFile) {
		int n = mySensorArray.length;
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(theFile, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//create new random object
		for(int i = 0; i < n - 1; i++) {
			writer.print(mySensorArray[i].getDate() + " ");
			writer.print(mySensorArray[i].getLocation() + " ");
			writer.print(mySensorArray[i].getRainRate() + " ");
			writer.print(mySensorArray[i].getTemperature() + " ");
			writer.print(mySensorArray[i].getPressure() + " ");
			writer.print(mySensorArray[i].getMoonPhase() + " ");
			writer.print(mySensorArray[i].getHumidity() + " ");
			writer.print(mySensorArray[i].getWindSpeed() + " ");
			writer.println();
		}
		writer.close();
	}
}
