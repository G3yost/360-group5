import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

class SensorTest {
	
	@Test
	void testDefaultConstructor() {
		
		Sensor sensor = new Sensor();
		
		assertEquals("DEFAULT", sensor.getLocation());
		assertEquals("CONSTRUCTOR", sensor.getMoonPhase());
		assertEquals(0, sensor.getRainRate());
		assertEquals(0, sensor.getTemperature());
		assertEquals(0, sensor.getPressure());
		assertEquals(0, sensor.getWindSpeed());
		assertEquals(0, sensor.getHumidity());
	}
	
	@Test
	void testGetDate() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals(time, sensor.getDate());
	}
	
	@Test
	void testGetLocation() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals("Washington", sensor.getLocation());
	}
	
	@Test
	void testMoonPhase() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals("Waining", sensor.moonPhase());
	}
	
	@Test
	void testGetRainRate() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals(0.7, sensor.getRainRate());
	}
	
	@Test
	void testGetTemperature() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals(6, sensor.getTemperature());
	}
	
	@Test
	void testGetPressure() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals(101325, sensor.getPressure());
	}
	
	@Test
	void testGetWindSpeed() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals(.87, sensor.getWindSpeed());
	}
	
	@Test
	void testGetHumidity() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals(76, sensor.getHumidity());
	}
	
	@Test
	void testGetMoonPhase() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		assertEquals("Waining", sensor.getMoonPhase());
	}
	
	@Test
	void testSetMoonPhase() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 6, 101325, .87, 76);
		
		
		assertEquals("Waining", sensor.getMoonPhase());
		sensor.setMoonPhase("Waxing");
		assertEquals("Waxing", sensor.getMoonPhase());
	}
	
	@Test
	void testConvertTemperatureUnit() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 0, 101325, .87, 76);
		
		sensor.convertTemperatureUnit("fahrenheit");
		sensor.convertTemperatureUnit("fahrenheit");
		assertEquals(32, sensor.getTemperature());
		
		sensor.convertTemperatureUnit("celcius");
		sensor.convertTemperatureUnit("celcius");
		assertEquals(0, sensor.getTemperature());
	}
	
	@Test
	void testConvertRainRateUnit() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 0, 101325, .87, 76);
		
		sensor.convertRainRateUnit("inches per hour");
		sensor.convertRainRateUnit("inches per hour");
		assertEquals(0.7 / 25.4, sensor.getRainRate());
		
		sensor.convertRainRateUnit("millimetres per hour");
		sensor.convertRainRateUnit("millimetres per hour");
		assertEquals(0.7, sensor.getRainRate());
	}
	
	@Test
	void testConvertPressureUnit() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 0, 101325, .87, 76);
		
		sensor.convertPressureUnit("pounds per square inch");
		sensor.convertPressureUnit("pounds per square inch");
		assertEquals(14.69594799225, sensor.getPressure());
		
		sensor.convertPressureUnit("pascals");
		sensor.convertPressureUnit("pascals");
		assertEquals(101325, sensor.getPressure());
	}
	
	@Test
	void testConvertWindSpeetUnit() {
		
		Instant time = Instant.now();
		Sensor sensor = new Sensor(time, "Washington", "Waining", 0.7, 0, 101325, .87, 76);
		
		sensor.convertWindSpeedUnit("knots");
		sensor.convertWindSpeedUnit("knots");
		assertEquals(1.6911593188710052, sensor.getWindSpeed());
		
		sensor.convertWindSpeedUnit("meters per second");
		sensor.convertWindSpeedUnit("meters per second");
		assertEquals(.87, sensor.getWindSpeed());
	}
	
	@Test
	void testCompareTo() {
		
		Instant time = Instant.now();
		Sensor sensor1 = new Sensor(time, "Washington", "Waining", 0.7, 0, 101325, .87, 76);
		Sensor sensor2 = new Sensor(time, "Washington", "Waining", 0.5, 0, 101325, .87, 76);
		Sensor sensor3 = new Sensor(time, "Washington", "Waining", 0.9, 0, 101325, .87, 76);
		
		assertEquals(-1, sensor1.compareTo(sensor3));
		assertEquals(0, sensor1.compareTo(sensor1));
		assertEquals(1, sensor1.compareTo(sensor2));
	}
}
